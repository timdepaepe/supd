# aifoon - Pure Data - Swarm Unit

### General

Application written in Pure Data for Raspberry Pi 3.
Change the versions file when changes were made.

Written by Tim De Paepe (tim.depaepe@gmail.com)